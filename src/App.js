import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import AccountPage from './components/Account/AccountPage.js';

function App() {
  return <><AccountPage /></>;
}

export default App;
