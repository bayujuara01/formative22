import { useState } from "react";
import { Card, Button, Row, Col, Form } from "react-bootstrap";

const AccountRegister = ({ submitHandler, modalControl }) => {
    const [user, setUser] = useState({ name: "", age: "" });

    const changeHandler = (event) => {
        const changedUser = {};
        changedUser[event.target.name] = event.target.value;
        setUser({
            ...user,
            ...changedUser
        });
    }

    const checkInputValidity = () => {
        if (!user.name && (user.age < 1)) {
            modalControl("Masukkan Salah", "Tolong, masukkan nama dengan benar & umur lebih dari 0");
            return;
        }

        if (!user.name && user.name.length < 1) {
            modalControl("Masukkan  Salah", "Masukkan nama tidak boleh kosong");
            return;
        }

        if (user.age < 1) {
            modalControl("Masukkan Salah", "Masukkan age tidak boleh kosong & harus lebih dari 0");
            return;
        }

        submitHandler(user);
    }

    return (
        <Card className="mx-auto user-card" style={{ width: '24rem', borderRadius: '8px' }}>

            <Card.Body>
                <Card.Title className="mt-4 mb-4">Masuk</Card.Title>
                <Form>
                    <Form.Group className="mb-4" controlId="formBasicEmail">
                        <Form.Label className="text-muted user-input__label">Nama</Form.Label>
                        <Form.Control
                            name="name"
                            type="text"
                            placeholder="Masukkan nama"
                            value={user.name}
                            onChange={changeHandler} />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicPassword">
                        <Form.Label className="text-muted user-input__label">Umur</Form.Label>
                        <Form.Control
                            name="age"
                            type="number"
                            placeholder="Masukkan Umur"
                            value={user.age}
                            onChange={changeHandler} />
                    </Form.Group>
                </Form>
                <Row className="mb-4">
                    <Col>
                        <Button style={{ width: '100%', borderRadius: '8px' }} variant="success" onClick={checkInputValidity}>Tambah</Button>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    );
}

export default AccountRegister;