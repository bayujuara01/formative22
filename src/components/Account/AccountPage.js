import AccountRegister from "./AccountRegister";
import { Col, Container, Row } from 'react-bootstrap';
import AccountModal from "./AccountModal";
import { useState } from "react";
import AccountList from "./AccountList";

const AccountPage = () => {
    const [listUser, setListUser] = useState([]);
    const [modal, setModal] = useState({ title: "", message: "" });
    const [show, setShow] = useState(false);

    const showModalControl = (title, message) => {
        setModal({
            ...modal,
            title,
            message
        });
        setShow(true)
    };
    const closeModalControl = () => setShow(false);

    const submitAccount = (user) => {
        setListUser([
            ...listUser,
            user
        ]);
    }

    return (
        <Container>
            <Row className="mb-4 mt-4">
                <Col>
                    <AccountRegister submitHandler={submitAccount} modalControl={showModalControl} />
                </Col>
            </Row>
            <Row>
                <Col>
                    <AccountList data={listUser} />
                </Col>
            </Row>


            <AccountModal show={show} onHide={closeModalControl} data={modal} />
        </Container>

    );
}

export default AccountPage;