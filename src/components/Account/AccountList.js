import { Card, Table } from "react-bootstrap";
import './account.css';

const AccountList = ({ data }) => {
    return (
        <Card className="mx-auto user-card">

            <Card.Body>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Age</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((user, index) => (<tr key={index}>
                            <td>{index + 1}</td>
                            <td>{user.name}</td>
                            <td>{user.age}</td>
                        </tr>))}
                    </tbody>
                </Table>
            </Card.Body>

        </Card>
    );
}

export default AccountList;