import { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';

const AccountModal = ({ show, onHide, data }) => {

    return (
        <Modal show={show} onHide={onHide} size="lg" >
            <Modal.Header closeButton>
                <Modal.Title>{data.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>{data.message}</Modal.Body>
            <Modal.Footer>
                <Button variant="success" onClick={onHide}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default AccountModal;