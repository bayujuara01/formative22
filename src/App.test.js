import { render, screen } from '@testing-library/react';
import App from './App';
import AccountPage from './components/Account/AccountPage'

test('renders learn react link', () => {
  render(<AccountPage />);
  const linkElement = screen.getByText(/Masuk/i);
  expect(linkElement).toBeInTheDocument();
});
